﻿using UnityEngine;

public class RigidBody2DLimitingComponent : MonoBehaviour
{
    //Methods
    private void Awake() {
        XUtils.check(_rigidBody);
    }

    private void FixedUpdate()  {
        if (_rigidBody) {
            float theRigidBodyVelocityLength = _rigidBody.velocity.magnitude;
            if (theRigidBodyVelocityLength > _maxVelocity)
                _rigidBody.velocity = _rigidBody.velocity / theRigidBodyVelocityLength * _maxVelocity;

            float theAngularVelocity = _rigidBody.angularVelocity;
            if (Mathf.Abs(theAngularVelocity) > _maxAngularVelocity)
                _rigidBody.angularVelocity =
                    (_rigidBody.angularVelocity > 0f) ? _maxAngularVelocity : -_maxAngularVelocity;
        }
    }

    //Fields
    [SerializeField] private Rigidbody2D _rigidBody = null;
    [SerializeField] private float _maxVelocity = 10f;
    [SerializeField] private float _maxAngularVelocity = 10f;
}
