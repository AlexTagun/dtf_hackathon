﻿using UnityEngine;

public class ObjectsDestroyer : MonoBehaviour
{

    private void Start()
    {
        spawnManager = FindObjectOfType<SpawnManager>();
    }

    private void OnCollisionEnter2D(Collision2D inCollision) {
        Asteroid asteroid = inCollision.gameObject.GetComponent<Asteroid>();
        if (asteroid)
        {
            if (!asteroid.HasExploded())
            {
                asteroid.Explode();
            }
        } else if (inCollision.gameObject.CompareTag("AsteroidPart"))
        {
            inCollision.gameObject.SetActive(false);
            Transform parent = inCollision.transform.parent;
            bool hasParts = false;
            for (int i = 0; i < parent.childCount; ++i)
            {
                var child = parent.GetChild(i);
                if (child.CompareTag("AsteroidPart") && child.gameObject.activeSelf)
                {
                    hasParts = true;
                }
            }
            if(!hasParts)
            {
                spawnManager.TryDestroy(parent.gameObject);
                SpawnAnimPrefab(parent.gameObject.transform);
            }
        }
        else if (XUtils.isValid(gameObject.GetComponent<world_exploring.Scripts.world_exploring.SpawnObjectView>()))
        {
            spawnManager.TryDestroy(inCollision.gameObject);
            SpawnAnimPrefab(inCollision.gameObject.transform);
        }
        else
        {
            XUtils.destroy(inCollision.gameObject);
            SpawnAnimPrefab(inCollision.gameObject.transform);
        }
    }

    private void SpawnAnimPrefab(Transform tran) {
        GameObject obj = Instantiate(_destroyAnimPrefab);
        obj.transform.position = tran.position;
        obj.transform.rotation = tran.gameObject.GetComponentInChildren<SpriteRenderer>().gameObject.transform.rotation;
        obj.transform.localScale = tran.gameObject.GetComponentInChildren<SpriteRenderer>().gameObject.transform.localScale;
        Sprite sprite = tran.gameObject.GetComponentInChildren<SpriteRenderer>().sprite;
        obj.GetComponent<SpriteRenderer>().sprite = sprite;
        obj.GetComponentInChildren<SpriteRenderer>().material.SetTexture("Texture2D_5DC9244A", sprite.texture);
    }

    private SpawnManager spawnManager = null;
    [SerializeField] private GameObject _destroyAnimPrefab = null;
}
