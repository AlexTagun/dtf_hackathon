﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoseScreen : MonoBehaviour
{
    [SerializeField] private Button _startButton = null;
    [SerializeField] private Button _exitButton = null;

    private void Awake()
    {
        _startButton.onClick.AddListener(GoToGameScene);
        _exitButton.onClick.AddListener(QuitGame);
    }

    public static void GoToGameScene()
    {
        SceneManager.LoadScene("MainScene");
    }
    public static void QuitGame()
    {
        Application.Quit();
    }
}
