﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject stationPrefab = null;

    private Station activeStation = null;

    private void Start()
    {
        
    }

    private void FixedUpdate()
    {
        if(activeStation == null)
        {
            activeStation = Instantiate(stationPrefab).GetComponent<Station>();
            Vector2 vector2 = Quaternion.Euler(0.0f, 0.0f, Random.Range(135, 135 + 90)) * FindObjectOfType<ShipMovement>().transform.position;
            activeStation.transform.position = new Vector3(vector2.x, vector2.y, -0.5f);
            activeStation.cameraAngle = Random.Range(0.0f, 360.0f);
        }
    }
}
