﻿using UnityEngine;

[ExecuteInEditMode]
public class Station : MonoBehaviour
{
    //[SerializeField]
    //public float captureRadius = 10.0f;

    [SerializeField]
    public float cameraAngle = 0.0f;


    [SerializeField]
    private float waitSpan = 2.0f;
    [SerializeField]
    private float rotationSpan = 2.0f;
    [SerializeField]
    private float distanceFromHole = 8.0f;

    private float expansionSpeed = 0.0f;


    private bool visited = false;
    private float waitTimer = 1.0f;
    private float rotationTimer = 1.0f;
    private float startRotation = 0.0f;
    private float startCameraRotation = 0.0f;
    private ShipStationLandable landable = null;
    private Vector2 blackHolePosition;
    private SpriteRenderer spriteRenderer;

    private Camera mainCamera;
    private new Rigidbody2D rigidbody;
    BlackHoleObject blackHole;

    public bool IsShipDocking()
    {
        return landable != null;
    }

    private void Awake()
    {
        
    }

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        blackHole = FindObjectOfType<BlackHoleObject>();
        mainCamera = Camera.main;
        rigidbody = GetComponent<Rigidbody2D>();
        expansionSpeed = blackHole.GetExpansionSpeed();
        blackHolePosition = blackHole.transform.position;
        startRotation = Mathf.Rad2Deg * Mathf.Atan2(transform.position.normalized.y - blackHolePosition.y, transform.position.normalized.x - blackHolePosition.x);
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, startRotation);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShipStationLandable currentLandable = collision.GetComponent<ShipStationLandable>();
        if (currentLandable
            && !currentLandable.IsLanding()
            && !visited
            && XUtils.isValid(mainCamera))
        {
            landable = currentLandable;
            visited = true;
            startCameraRotation = mainCamera.transform.rotation.eulerAngles.z;
            waitTimer = 0.0f;
            rotationTimer = 0.0f;
            landable.PerformLanding(this);
        }
    }

    private void FixedUpdate()
    {
        blackHolePosition = blackHole.transform.position;

        if (IsShipDocking())
        {
            if(waitTimer != 1.0f)
            {
                waitTimer = Mathf.MoveTowards(waitTimer, 1.0f, Time.fixedDeltaTime / waitSpan);
            } else
            {
                rotationTimer = Mathf.MoveTowards(rotationTimer, 1.0f, Time.fixedDeltaTime / rotationSpan);

                float newRotation = Mathf.LerpAngle(startRotation, cameraAngle, rotationTimer);
                rigidbody.rotation = newRotation;
                mainCamera.transform.rotation = Quaternion.Euler(0.0f, 0.0f, newRotation - startRotation + startCameraRotation);

                if(rotationTimer == 1.0f)
                {
                    landable = null;
                }
            }
        }

        rigidbody.position = Quaternion.Euler(0.0f, 0.0f, startRotation) * new Vector2(blackHole.GetRadius() + distanceFromHole, 0.0f);
          
        if(IsShipDocking() && !landable.IsLanding())
        {
            landable.transform.rotation = Quaternion.Euler(0.0f, 0.0f, transform.rotation.eulerAngles.z - 90.0f);
            landable.transform.position = new Vector3(transform.position.x, transform.position.y, landable.transform.position.z);
        }

        if(visited && landable == null)
        {
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, spriteRenderer.color.a * 0.99f);
        }
    }


}
