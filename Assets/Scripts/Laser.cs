﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [SerializeField] float laserChargeCount;
    [SerializeField] float EnergyCost;
    [SerializeField] float MaxEnergy;
    [SerializeField] float timeExistenceLazer = 0f;

    [SerializeField] AudioSource soundLowLaserCharge;

    private LineRenderer lineRenderer = null;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        lineRenderer.useWorldSpace = true;

        MaxEnergy = laserChargeCount;
    }

    public float LaserChargeCount() {
        return laserChargeCount;
    }

    public float GetMaxLaserChargeCount() {
        return MaxEnergy;
    }

    public void RegenLaserCharge()
    {
        laserChargeCount = MaxEnergy;
    }

    public void DrawLazer(Vector3 firstPos, Vector3 secondPos) {
        lineRenderer.SetPosition(0, firstPos);
        lineRenderer.SetPosition(1, secondPos);
        lineRenderer.enabled = true;
        offLaser();
    }
    void offLaser() {
        StartCoroutine(WaitOffLaser());
    }
    IEnumerator WaitOffLaser() {
        yield return new WaitForSeconds(timeExistenceLazer);
        lineRenderer.enabled = false;
    }
    public bool isLaserCharged () {
        if (laserChargeCount > EnergyCost) {
            return true;
        } else {
            soundLowLaserCharge.PlayOneShot(soundLowLaserCharge.clip);
            return false;
        }
    }
    public void chargeReduction() {
        if (EnergyCost < laserChargeCount) {
            laserChargeCount -= EnergyCost;
        }
    }
}
