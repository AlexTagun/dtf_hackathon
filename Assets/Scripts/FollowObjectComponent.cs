﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObjectComponent : MonoBehaviour {
    private GameObject _objectToFollow;
    private Rigidbody2D _rigitbody;
    [SerializeField] private float _followSpeed = 10;

    private void Start() {
        _objectToFollow = GameObject.FindGameObjectWithTag("Player");
        _rigitbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (XUtils.isValid(gameObject) && XUtils.isValid(_objectToFollow))
        {
            Vector3 heading = _objectToFollow.transform.position - gameObject.transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            _rigitbody.velocity = direction * _followSpeed;
        }
    }
}
