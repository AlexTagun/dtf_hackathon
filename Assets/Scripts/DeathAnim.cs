﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAnim : MonoBehaviour {

    private void Start() {
        StartCoroutine(DeactivateObject());
    }
    private IEnumerator DeactivateObject() {
        //Debug.Log("dfdfdf");
        float value = 1.54f; //TODO
        float valueSpeed = 3f;
        while (value > 0) {
            value -= valueSpeed * Time.deltaTime;
            gameObject.GetComponentInChildren<SpriteRenderer>().material.SetFloat("Vector1_236FED64", value);
            yield return null;
        }
        Destroy(gameObject);
    }
}
