﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TrainingImageStorage : MonoBehaviour {
    [SerializeField] private List<Sprite> _imageList;
    [SerializeField] private Image _image;
    [SerializeField] private Button _prevButton;
    [SerializeField] private Button _nextButton;
    [SerializeField] private Button _startButton;
    private int _curIndex = 0;

    private void Start() {
        //_prevButton.onClick.AddListener(delegate { Prev(); });
        //_nextButton.onClick.AddListener(delegate { Next(); });
        _startButton.onClick.AddListener(delegate { SceneManager.LoadScene("MainScene"); });

        //SetSparite();
        //_prevButton.gameObject.SetActive(false);
    }

    


    public void Next() {
        _prevButton.gameObject.SetActive(true);
        if (_curIndex >= _imageList.Count - 1) {
            _nextButton.gameObject.SetActive(false);
            _startButton.gameObject.SetActive(true);
            return;
        }
        _curIndex++;
        SetSparite();
    }

    public void Prev() {
        _nextButton.gameObject.SetActive(true);
        _startButton.gameObject.SetActive(false);
        if (_curIndex == 0) {
            return;
        }
        if(_curIndex == 1) {
            _prevButton.gameObject.SetActive(false);
        }
        _curIndex--;
        SetSparite();
    }

    private void SetSparite() {
        _image.sprite = _imageList[_curIndex];
    }

}
