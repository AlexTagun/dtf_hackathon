﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour {
    [SerializeField] private Button _startButton = null;
    //[SerializeField] private Button _exitButton = null;

    private void Awake() {
        _startButton.onClick.AddListener(GoToGameScene);
        //_exitButton.onClick.AddListener(OnClickExit);
    }

    public static void GoToGameScene() {
        SceneManager.LoadScene("MainScene");
    }

    public void OnClickExit() {
        Application.Quit();
    }
}
