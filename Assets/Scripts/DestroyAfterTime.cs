﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
    [SerializeField] private float _timeToWait = 0.9f;
    private void Start() {
        StartCoroutine(DestroyObject());
    }
    private IEnumerator DestroyObject() {
        yield return new WaitForSeconds(_timeToWait);
        Destroy(gameObject);
    }
}
