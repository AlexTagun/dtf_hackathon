﻿using System.Linq;
using UnityEngine;
using world_exploring.Scripts.world_exploring;

public class Asteroid : SpawnObjectView {

    private GameObject[] children;
    [Header("asteroid parts")]
    [SerializeField] private float speedPartAsteroid = 0f;
    [SerializeField] private float rotationPartAsteroid = 0f;
    [SerializeField] private float speedMiddlePartAsteroid = 0f;
    [SerializeField] private float rotationMiddlePartAsteroid = 0f;
    [SerializeField] private Vector3[] _childrenLocalPos = new Vector3[3];
    [SerializeField] private Quaternion[] _childrenLocalRotation = new Quaternion[3];
    [SerializeField] private AudioSource soundRockExplode = null;

    private bool exploded = false;
    private Rigidbody2D _rb = null;

    public bool HasExploded()
    {
        return exploded;
    }

    void Start() {
        _rb = GetComponent<Rigidbody2D>();
        children = new GameObject[transform.childCount - 1];
        for(int i = 1; i < transform.childCount; ++i)
        {
            children[i - 1] = transform.GetChild(i).gameObject;
            _childrenLocalPos[i - 1] = children[i - 1].transform.localPosition;
            _childrenLocalRotation[i - 1] = children[i - 1].transform.localRotation;
        }
    }
    
    public override void Clear() {
        
        base.Clear();
        if (exploded) {
            SetRigidBodyEnabled(true);
            ResetChildren();

            Debug.Assert(exploded);
            exploded = false;
            GetComponent<CircleCollider2D>().enabled = true;
            children[0].GetComponent<PolygonCollider2D>().enabled = false;
            Destroy(children[0].GetComponent<Rigidbody2D>());
            children[1].GetComponent<PolygonCollider2D>().enabled = false;
            Destroy(children[1].GetComponent<Rigidbody2D>());
            children[2].GetComponent<PolygonCollider2D>().enabled = false;
            Destroy(children[2].GetComponent<Rigidbody2D>());
        }

        exploded = false;
        
    }

    private void SetRigidBodyEnabled(bool value) {
        for (int i = 1; i < transform.childCount; ++i) {
            children[i - 1].SetActive(true);
        }
        if (value) {
            if (_rb == null) {
                _rb = gameObject.AddComponent<Rigidbody2D>();
            }
        } else {
            if (_rb != null) {
                Destroy(_rb);
            }
        }
    }

    public void Explode () {
        exploded = true;
        SetRigidBodyEnabled(false);
        GetComponent<CircleCollider2D>().enabled = false;
        soundRockExplode.PlayOneShot(soundRockExplode.clip);

        var forces = new[]{Vector2.left * speedPartAsteroid, Vector2.up * speedMiddlePartAsteroid, Vector2.right * speedPartAsteroid};
        var rotations = new[]{rotationPartAsteroid, rotationMiddlePartAsteroid, -rotationPartAsteroid};

        for (int i = 0; i < children.Length; i++) {
            children[i].GetComponent<PolygonCollider2D>().enabled = true;
            var rb = children[i].GetComponent<Rigidbody2D>();
            rb = rb ? rb : children[i].AddComponent<Rigidbody2D>();
            rb.AddForce(forces.ElementAtOrDefault(i), ForceMode2D.Impulse);
            rb.AddTorque(rotations.ElementAtOrDefault(i));
        }
    }
    public void ExplodePartAsteroid(GameObject partAsteroid) {
        partAsteroid.SetActive(false);
        soundRockExplode.PlayOneShot(soundRockExplode.clip);
    }

    private void ResetChildren() {
        for (int i = 1; i < transform.childCount; ++i) {
            children[i - 1].transform.localPosition = _childrenLocalPos[i - 1];
            children[i - 1].transform.localRotation = _childrenLocalRotation[i - 1];
            Debug.Log(children[i - 1].transform.localPosition);

            children[i - 1].GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            Debug.Log(children[i - 1].GetComponent<Rigidbody2D>().velocity);


        }
    }
}
