﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAttach : MonoBehaviour
{
    //Methods
    void FixedUpdate() {
        Vector3 thePosition = transform.position;
        thePosition.z = _camera.transform.position.z;
        _camera.transform.position = thePosition;
    }

    //Fields
    [SerializeField] private Camera _camera = null;
}
