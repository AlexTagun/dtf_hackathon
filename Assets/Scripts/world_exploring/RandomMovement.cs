﻿using UnityEngine;

public class RandomMovement : MonoBehaviour {
	private float tChange = 0; // force new direction in the first Update
	private float randomX;
	private float randomY;
	public float moveSpeed = 5;

	void Update() {
		// change to random direction at random intervals
		if (Time.time >= tChange) {
			randomX = Random.Range(-2.0f, 2.0f); // with float parameters, a random float
			randomY = Random.Range(-2.0f, 2.0f); //  between -2.0f and 2.0f is returned
			tChange = Time.time + Random.Range(0.5f, 1.5f);
		}

		transform.Translate(new Vector3(randomX, randomY, 0) * moveSpeed * Time.deltaTime);
	}
}