﻿using System;
 using UnityEngine;
 
 namespace world_exploring.Scripts.world_exploring {
 	[CreateAssetMenu(fileName = "explore_settings", menuName = "Settings/Explore", order = 1)]
 	public class WorldExploreSettings : ScriptableObject {
 		public int SpawnChunkSize = 10;

 		[Header("spawn object settings")]
 		public int LiveDistanceInChunks = 1;
 		[Header("levels")]
 		public ExploreWorldLevel[] Levels;
 	}
 
 	[Serializable]
 	public class ExploreWorldLevel {
 		public float Distance = 20;
        [Header("spawn object settings")]
        [Space(10)]
        public int PhysicObjectsInChunk = 2;
 		
 		public SpawnObjectView[] PhysicObjects;
        [Space(10)]
        public int BackgroundObjectsInChunk = 2;

 		public SpawnObjectView[] BackgroundObjects;
 	}
 }