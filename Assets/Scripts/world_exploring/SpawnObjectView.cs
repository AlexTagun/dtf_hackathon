﻿using UnityEngine;

namespace world_exploring.Scripts.world_exploring {
    public class SpawnObjectView : MonoBehaviour {
        public string PoolId;
        public bool DestroyOnDeactivate = true;

        [Header("visual randomize")] 
        [SerializeField] public float MinScale = 1;
        [SerializeField] public float MaxScale = 1;

        public virtual void Clear() {
            
        }

        public virtual void Initialize() {
            SetRandomScale();
        }

        private void SetRandomScale() {
            var scale = Random.Range(MinScale, MaxScale);
            transform.localScale = Vector3.one * scale;
        }
    }
}
