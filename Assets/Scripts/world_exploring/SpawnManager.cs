﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using pool;
using UnityEngine;
using world_exploring.Scripts.world_exploring;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour {
	[SerializeField] private Transform _player = null;

	[SerializeField] private Transform _startPoint = null;

	[SerializeField] private WorldExploreSettings _exploreSettings = null;

	private float _distanceToStart = 0f;
	private ExploreWorldLevel[] _exploreLevels = null;
	private ExploreWorldLevel _currentExploreLevel = null;
	private Vector2 _currentChunk;
	private List<SpawnObjectView> _spawnedObjects = new List<SpawnObjectView>();
	private ComplexComponentObjectPool<string, SpawnObjectView> _complexObjectsPool = null;
	private List<Vector2> _cachedChunks = new List<Vector2>();

	// Start is called before the first frame update
	private void Awake() {
		_exploreLevels = _exploreSettings.Levels.ToList().OrderBy(x => x.Distance).ToArray();
		_currentChunk = GetChunkByPosition(_player.position);
		_complexObjectsPool = new ComplexComponentObjectPool<string, SpawnObjectView>(CreateSpawnObject);
		TryUpdateLevel();
		TrySpawnObjects(true);
	}

	private SpawnObjectView CreateSpawnObject(object[] arg) {
		var prefab = arg[0] as SpawnObjectView;
		var prefabId = prefab.name;
		var result = Instantiate(prefab);
		result.PoolId = prefabId;
		return result;
	}

	private Vector2 GetChunkByPosition(Vector3 position) {
		var x = Mathf.Floor(position.x / _exploreSettings.SpawnChunkSize);
		var y = Mathf.Floor(position.y / _exploreSettings.SpawnChunkSize);
		return new Vector2(x, y);
	}

	// Update is called once per frame
	void Update() {
        if (XUtils.isValid(_player))
		    _distanceToStart = Vector2.Distance(_player.position, _startPoint.position);
		
		TryUpdateLevel();
		TrySpawnObjects();
	}

	private void TryClearChunks() {
		for (int i = _cachedChunks.Count - 1; i >= 0; i--) {
			var deltaX = _currentChunk.x - _cachedChunks[i].x;
			var deltaY = _currentChunk.y - _cachedChunks[i].y;
			if (Mathf.Abs(deltaX) > _exploreSettings.LiveDistanceInChunks || Mathf.Abs(deltaY) > _exploreSettings.LiveDistanceInChunks) {
			   	_cachedChunks.RemoveAt(i);
			}
		}
	}

	private void TryClearObjects() {
		for (int i = _spawnedObjects.Count - 1; i >= 0; i--) {
			var spawnedObject = _spawnedObjects[i];
			if(XUtils.isValid(spawnedObject))
            {
                var chunk = GetChunkByPosition(spawnedObject.transform.position);
                if (!_cachedChunks.Contains(chunk))
                {
	                _spawnedObjects.RemoveAt(i);

	                if (spawnedObject.DestroyOnDeactivate) {
		                Destroy(spawnedObject.gameObject);
	                } else {
		                spawnedObject.Clear();
                        _complexObjectsPool.Release(spawnedObject, spawnedObject.PoolId);
	                }
                }
            }
		}
	}

    public void TryDestroy(GameObject gameObject)
    {
        int id = -1;
        for(int i = 0; i < _spawnedObjects.Count; ++i)
        {
            if(_spawnedObjects[i] == gameObject.GetComponent<SpawnObjectView>())
            {
                id = i;
                break;
            }
        }
        if(id == -1)
        {
            return;
        }

        var spawnedObject = _spawnedObjects[id];
        _spawnedObjects.RemoveAt(id);
		
        //StartCoroutine(DeactivateObject(spawnedObject));
    }

	private void TrySpawnObjects(bool onInitialize = false) {
        if (!XUtils.isValid(_player)) return;

		var currentChunk = GetChunkByPosition(_player.position);

		if (onInitialize) {
			SpawnObjectsInChunk(currentChunk);
			_cachedChunks.Add(currentChunk);
		}

		if (onInitialize || _currentChunk != currentChunk) {
			_currentChunk = currentChunk;
			
			TryClearChunks();
			TryClearObjects();
			
			var newChunks = new List<Vector2>();
			newChunks.Add(new Vector2(currentChunk.x - 1, currentChunk.y));
			newChunks.Add(new Vector2(currentChunk.x + 1, currentChunk.y));
			newChunks.Add(new Vector2(currentChunk.x + 1, currentChunk.y + 1));
			newChunks.Add(new Vector2(currentChunk.x + 1, currentChunk.y - 1));
			newChunks.Add(new Vector2(currentChunk.x - 1, currentChunk.y - 1));
			newChunks.Add(new Vector2(currentChunk.x - 1, currentChunk.y + 1));
			newChunks.Add(new Vector2(currentChunk.x, currentChunk.y - 1));
			newChunks.Add(new Vector2(currentChunk.x, currentChunk.y + 1));

			for (int j = 0; j < newChunks.Count; j++) {
				if (_cachedChunks.Contains(newChunks[j])) {
					continue;
				}

				_cachedChunks.Add(newChunks[j]);

				SpawnObjectsInChunk(newChunks[j]);
			}
		}
	}

	private void SpawnObjectsInChunk(Vector2 chunk) {
		for (int i = 0; i < _currentExploreLevel.PhysicObjectsInChunk; i++) {
			SpawnObjectView newObject = null;
			var success = SpawnObject(ref newObject, _currentExploreLevel.PhysicObjects);
			if (success) {
				var position = GetNewSpawnObjectPosition(chunk);
				newObject.transform.position = position;

                newObject.gameObject.SetActive(true);
                //newObject.GetComponentInChildren<SpriteRenderer>().material.SetFloat("Vector1_236FED64", 1.54f); //TODO
            }
		}
		
		for (int i = 0; i < _currentExploreLevel.BackgroundObjectsInChunk; i++) {
			SpawnObjectView newObject = null;
			var success = SpawnObject(ref newObject, _currentExploreLevel.BackgroundObjects);
			if (success) {
				var position = GetNewSpawnObjectPosition(chunk);
				newObject.transform.position = position;

				newObject.gameObject.SetActive(true);
			}
		}
	}

	private bool SpawnObject(ref SpawnObjectView result, SpawnObjectView[] collection) {
		var randomIndex = Random.Range(0, collection.Length);
		var prefab = collection.ElementAtOrDefault(randomIndex);
		if (prefab != null) {
			var instance = _complexObjectsPool.Get(prefab.name, true, prefab);
			instance.Initialize();
			_spawnedObjects.Add(instance);
			result = instance;
			return true;
		}

		return false;
	}

	private Vector3 GetNewSpawnObjectPosition(Vector2 chunk) {
		var minX = chunk.x * _exploreSettings.SpawnChunkSize;
		var maxX = minX + _exploreSettings.SpawnChunkSize;

		var minY = chunk.y * _exploreSettings.SpawnChunkSize;
		var maxY = minY + _exploreSettings.SpawnChunkSize;

		var x = Random.Range(minX, maxX);
		var y = Random.Range(minY, maxY);

		var position = new Vector3(x, y);
		return position;
	}

	private void TryUpdateLevel() {
		ExploreWorldLevel newLevel = null;
		int levelIndex = 0;
		for (int i = 0; i < _exploreLevels.Length; i++) {
			newLevel = _exploreLevels[i];
			levelIndex = i;

			if (i == _exploreLevels.Length - 1) {
				break;
			}


			if (_exploreLevels[i + 1].Distance > _distanceToStart) {
				break;
			}
		}

		if (newLevel != _currentExploreLevel) {
			_currentExploreLevel = newLevel;
		}
	}
	
	private IEnumerator DeactivateObject(SpawnObjectView obj) {
		float value = 1.54f; //TODO
		float valueSpeed = 5f;
		while (value > 0) {
			value -= valueSpeed * Time.deltaTime;
			obj.GetComponentInChildren<SpriteRenderer>().material.SetFloat("Vector1_236FED64", value);
			yield return null;
		}

		if (obj.DestroyOnDeactivate) {
			Destroy(obj.gameObject);
		} else {
			obj.Clear();
			_complexObjectsPool.Release(obj, obj.PoolId); 
		}
	}
}