﻿using UnityEngine;

public class ShipStationLandable : MonoBehaviour
{
    [SerializeField]
    private float landingSpan = 2.0f;

    private bool isLanding = false;
    private Station landingStation = null;
    private float landingTimer = 1.0f;

    private Quaternion startRotation;
    private Vector2 startPosition;

    private new Rigidbody2D rigidbody;
    private ShipMovement shipMovement;

    public bool IsLanding()
    {
        return isLanding;
    }

    public void PerformLanding(Station inStation)
    {
        Debug.Assert(!isLanding);

        isLanding = true;
        landingStation = inStation;
        startRotation = transform.rotation;
        startPosition = transform.position;
        landingTimer = 0.0f;
        shipMovement.SetCanMove(false);
        shipMovement.RefillFuel();
    }

    private void Awake()
    {
        shipMovement = GetComponent<ShipMovement>();
        rigidbody = GetComponentInChildren<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if(isLanding)
        {
            landingTimer = Mathf.MoveTowards(landingTimer, 1.0f, Time.fixedDeltaTime / landingSpan);

            rigidbody.rotation = Quaternion.Lerp(startRotation, landingStation.transform.rotation * Quaternion.Euler(0.0f, 0.0f, -90.0f), landingTimer).eulerAngles.z;
            Vector2 vector2 = Vector2.Lerp(startPosition, landingStation.transform.position, landingTimer);
            rigidbody.position = new Vector3(vector2.x, vector2.y, transform.position.z);

            if (landingTimer == 1.0f)
            {
                isLanding = false;
            }
        }

        if(landingStation != null)
        {
            if(!landingStation.IsShipDocking())
            {
                shipMovement.SetCanMove(true);
                shipMovement.increaseAcceleratorEnergy();
                gameObject.GetComponentInChildren<Laser>().RegenLaserCharge();
                Destroy(landingStation.gameObject, 1.0f);
                landingStation = null;
            }
        }
    }
}
