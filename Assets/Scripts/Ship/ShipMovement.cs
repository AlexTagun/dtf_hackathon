﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipMovement : MonoBehaviour
{
    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    private float acceleration = 0.5f;
    [SerializeField]
    private float superForceMultiplier = 2.0f;
    [SerializeField]
    private float superFuelMultiplier = 2.0f;
    [SerializeField]
    private float rotationSpeed = 45.0f;
    
    [SerializeField] private float maxFuel = 100f;
    [SerializeField] private Values.LimitedFloat fuelValue;
    [SerializeField] private float fuelForMove = 1f;

    [SerializeField] FuelContainer fuelContainer;
    [SerializeField] private ParticleSystem engineParticleSystem = null;

    [SerializeField]
    private float refillSpan = 2.0f;
    [SerializeField]
    private float refillRatio = 0.25f;
    [SerializeField]
    private int acceleratorEnergyMax = 6;

    private int acceleratorEnergy = 0;

    private bool canMove = true;

    private new Rigidbody2D rigidbody;
    private Vector2 forwardVector;
    private Camera mainCamera;
    private float refillTimer = 1.0f;

    private string debugText;

    public Values.LimitedFloat getFuelValue() {
        return fuelValue;
    }

    public void RefillFuel()
    {
        refillTimer = 0.0f;
    }

    public void SetCanMove(bool inCanMove)
    {
        canMove = inCanMove;
    }

    public int getAcceleratorEnergy()
    {
        return acceleratorEnergy;
    }

    public void increaseAcceleratorEnergy()
    {
        ++acceleratorEnergy;
        if(acceleratorEnergy == acceleratorEnergyMax)
        {
            showWin();
        }
    }

    public void showWin()
    {
        StartCoroutine(showWinCoroutine());
    }

    IEnumerator showWinCoroutine()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("WinScene");
    }

    public int getAcceleratorEnergyMax()
    {
        return acceleratorEnergyMax;
    }

    private void Awake()
    {
        mainCamera = Camera.main;
        rigidbody = GetComponentInChildren<Rigidbody2D>();

        fuelValue = new Values.LimitedFloat(0f, maxFuel, maxFuel);

        XUtils.check(engineParticleSystem);
    }

    private void FixedUpdate()
    {
        UpdateMovement();

        if(refillTimer != 1.0f)
        {
            refillTimer = Mathf.MoveTowards(refillTimer, 1.0f, Time.fixedDeltaTime / refillSpan);
            fuelValue.setValue(fuelValue.getValue() + fuelValue.getMaximum() * refillRatio * Time.fixedDeltaTime / refillSpan);
        }
    }
    private void OnGUI()
    {
        GUILayout.Label(debugText);
    }

    private float CalculateInputAngle()
    {
        Vector2 inputMouse = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 cameraPosition = mainCamera.transform.position;
        Vector2 worldMouse = inputMouse + cameraPosition;
        Vector2 deltaPosition = inputMouse - rigidbody.position;
        return Mathf.Rad2Deg * Mathf.Atan2(deltaPosition.y, deltaPosition.x);
    }

    private void Debug(params string[] Args)
    {
        debugText = "\t\t";
        foreach(string Arg in Args)
        {
            if(debugText != "\t\t")
            {
                debugText += "; ";
            }
            debugText += Arg; 
        }
    }

    private void UpdateMovement()
    {
        float inputForward = Input.GetAxis("Vertical");
        float inputSideways = Input.GetAxis("Horizontal");
        float inputRotation = CalculateInputAngle();
        float currentSpeed = rigidbody.velocity.magnitude;
        float currentRotation = rigidbody.rotation;
        float deltaRotation = inputRotation - rigidbody.rotation;
        Vector2 forward = Quaternion.Euler(0, 0, rigidbody.rotation) * new Vector2(1.0f, 0.0f);
        Vector2 inputVelocity = Mathf.Max(0.0f, inputForward) * speed * forward;

        Debug("inputForward: " + inputForward, "inputSideways: " + inputSideways, "currentSpeed: " + currentSpeed, "currentRotation: " + currentRotation, "deltaRotation: " + deltaRotation,
            "inputVelocity: " + inputVelocity);
                
        var theEmission = engineParticleSystem.emission;
        theEmission.enabled = 0f != inputForward && !fuelValue.isValueMinimum();

        if (canMove)
        {
            rigidbody.rotation = Mathf.MoveTowardsAngle(currentRotation, inputRotation, rotationSpeed * Time.fixedDeltaTime);
            if (inputForward > 0.0f && !fuelValue.isValueMinimum())
            {
                float multiplier = 1.0f;
                float fuelMultiplier = 1.0f;
                if (Input.GetAxis("Ctrl") > 0.0f || Input.GetAxis("Shift") > 0.0f || Input.GetAxis("Space") > 0.0f)
                {
                    multiplier = superForceMultiplier;
                    fuelMultiplier = superFuelMultiplier;
                }
                engineParticleSystem.playbackSpeed = fuelMultiplier;
                fuelValue.changeValue(-fuelForMove * inputForward * fuelMultiplier);
                //rigidbody.velocity = Vector2.MoveTowards(rigidbody.velocity, inputVelocity * multiplier, );

                rigidbody.AddForce(inputVelocity * acceleration * multiplier * Time.fixedDeltaTime * 10.0f);
            }
            rigidbody.angularVelocity = Mathf.Lerp(rigidbody.angularVelocity, 0.0f, 0.1f);
        }
        else
        {
            rigidbody.velocity = Vector2.zero;
            rigidbody.angularVelocity = 0.0f;
        }
        
    }

    private void OnDestroy() {
        var theFinalManager = Object.FindObjectOfType<FinalManager>();
        if (XUtils.isValid(theFinalManager))
            theFinalManager.showLoose();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "FuelContainer") {
            fuelValue.setValue(fuelValue.getValue() + fuelContainer.fuelValue);
            Destroy(collision.gameObject);
        }
    }
}
