﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour {
    [SerializeField] private Laser laser = null;
    [SerializeField] AudioSource soundLaserShot;
    Rigidbody2D rb2D = null;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            if (laser.isLaserCharged()) {
                laser.chargeReduction();
                soundLaserShot.PlayOneShot(soundLaserShot.clip);
                Vector3 mousePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePoint.z = 0;
                var heading = mousePoint - transform.position;
                var distance = heading.magnitude;
                var direction = heading / distance;
                RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, direction);
                DestroyAsteroid(ShootAtGameObject(hits, direction));
            }

        }
    }
   
    void DestroyAsteroid(GameObject Asteroid) {
        if (Asteroid != null) {
            if (Asteroid.tag == "Asteroid") {
                Asteroid.GetComponent<Asteroid>().Explode();
            }
            else if (Asteroid.tag == "AsteroidPart") {
                Asteroid.GetComponentInParent<Asteroid>().ExplodePartAsteroid(Asteroid);
            }
        }
    }
    GameObject ShootAtGameObject(RaycastHit2D[] hits, Vector3 direction) {
        if (hits.Length == 1 && hits[0].collider.gameObject.tag == "Player") {
            laser.DrawLazer(transform.position, transform.position + direction * 100f);
        }
        else for(int i = 0; i<hits.Length; i++) {
            if (hits[i].collider.gameObject.tag != "Player") {
                laser.DrawLazer(transform.position, transform.position + direction * hits[i].distance);
                    if (hits[i].collider.gameObject.tag == "AsteroidPart") {
                        return hits[i].collider.gameObject;
                    }
                    else if (hits[i].collider.gameObject.tag == "Asteroid") {
                        return hits[i].collider.gameObject;
                    }
                break;
            }
        }
        return null;
    }
}
