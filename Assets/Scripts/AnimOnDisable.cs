﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimOnDisable : MonoBehaviour {
    [SerializeField] private GameObject _animPrefab = null;
    [SerializeField] private GameObject _particlePrefab = null;


    private void OnDisable() {
        SpawnAnimPrefab(transform, _animPrefab);
        SpawnParticlePrefab(transform, _particlePrefab);
    }

    private void SpawnAnimPrefab(Transform tran, GameObject prefab) {
        if (!tran || !GetComponentInParent<Transform>()) return;
        GameObject obj = Instantiate(prefab);
        obj.transform.position = tran.position;
        obj.transform.localRotation = tran.localRotation;

        obj.transform.localScale = tran.GetComponentInParent<Transform>().localScale;
        Sprite sprite = tran.gameObject.GetComponent<SpriteRenderer>().sprite;
        obj.GetComponent<SpriteRenderer>().sprite = sprite;
        obj.GetComponentInChildren<SpriteRenderer>().material.SetTexture("Texture2D_5DC9244A", sprite.texture);
    }

    private void SpawnParticlePrefab(Transform tran, GameObject prefab) {
        if (!tran || !GetComponentInParent<Transform>()) return;
        GameObject obj = Instantiate(prefab);
        obj.transform.position = tran.position;
        obj.transform.localRotation = tran.localRotation;

        obj.transform.localScale = tran.GetComponentInParent<Transform>().localScale;
        
    }
}
