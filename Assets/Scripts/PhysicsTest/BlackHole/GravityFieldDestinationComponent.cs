﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityFieldDestinationComponent : MonoBehaviour
{
    //Methods
    private void Awake() {
        XUtils.check(_controlledRigidBody);

        GravityFieldSourceComponent[] theFieldSources = Object.FindObjectsOfType<GravityFieldSourceComponent>();
        foreach (GravityFieldSourceComponent theSource in theFieldSources)
            if (XUtils.isValid(theSource))
                theSource.registerNewCreatedDestination(this);
    }

    public Vector2 position { get { return transform.position; } }
    public Rigidbody2D controlledRigidBody { get { return _controlledRigidBody; } }

    //Fields
    [SerializeField] Rigidbody2D _controlledRigidBody = null;
}
