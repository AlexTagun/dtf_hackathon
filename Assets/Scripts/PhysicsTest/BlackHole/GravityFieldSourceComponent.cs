﻿using UnityEngine;

public class GravityFieldSourceComponent : MonoBehaviour
{
    //Methods
    //-API
    public float radiusOffset {
        set { _fieldRadiusOffset = value; }
    }

    //-Init
    private void Awake() {
        performInitialDestinationsRegistration();
    }

    //-Field updating
    private void FixedUpdate() {
        _fieldDestinations.iterateWithRemove((GravityFieldDestinationComponent inDestination)=>{
            if (XUtils.isValid(inDestination)) {
                updateDestination(inDestination);
                return false;
            } else {
                return true;
            }
        });

        ShipMovement ship = FindObjectOfType<ShipMovement>();
        if(ship)
        {
            Vector2 shipPos = ship.transform.position;
            Vector2 theFieldDistanceVector = position - shipPos;
            float theFieldDistance = Mathf.Max(0.0f, theFieldDistanceVector.magnitude - GetComponent<BlackHoleObject>().GetRadius() * 0.5f);

            GetComponent<AudioSource>().volume = 1.0f / Mathf.Pow(theFieldDistance + 1.0f, 1.6f);
        }
    }

    private void updateDestination(GravityFieldDestinationComponent inDestination) {
        Vector2 theFieldDistanceVector = (position - inDestination.position);
        float theFieldDistance = theFieldDistanceVector.magnitude;
        Vector2 theFieldDistanceDirection = theFieldDistanceVector / theFieldDistance;

        float theOffsetedFieldDistance = theFieldDistance - _fieldRadiusOffset;

        float theFieldForceMagnitude = getForceMagnitudeForDistance(theOffsetedFieldDistance);
        Rigidbody2D theRigidBody = inDestination.controlledRigidBody;

        if (theRigidBody) {
            float theMassFactor = _forceProgressiveToMass ? theRigidBody.mass : 1f;
            theRigidBody.AddForce(theFieldDistanceDirection * theFieldForceMagnitude * theMassFactor);
        }
    }

    private float getForceMagnitudeForDistance(float inDistance) {
        return _forceCurve.Evaluate(inDistance);
    }

    private Vector2 position { get { return transform.position; } }

    //-Destinations collecting
    private void performInitialDestinationsRegistration() {
        _fieldDestinations = new FastArray<GravityFieldDestinationComponent>();

        GravityFieldDestinationComponent[] theFieldDestinations = Object.FindObjectsOfType<GravityFieldDestinationComponent>();
        _fieldDestinations.assignFrom(theFieldDestinations);
    }

    public void registerNewCreatedDestination(GravityFieldDestinationComponent inDestination) {
        if (isInitialDestinationsRegistrationPerformed)
            _fieldDestinations.addUnique(inDestination);
    }

    private bool isInitialDestinationsRegistrationPerformed {
        get { return XUtils.isValid(_fieldDestinations); }
    }

    //Fields
    [SerializeField] AnimationCurve _forceCurve = null;
    [SerializeField] float _fieldRadiusOffset = 0f;
    [SerializeField] bool _forceProgressiveToMass = true;

    FastArray<GravityFieldDestinationComponent> _fieldDestinations = null;
}
