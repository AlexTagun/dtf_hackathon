﻿using UnityEngine;

public class BlackHoleObject : MonoBehaviour
{
    private Material material;

    private void Awake() {
        XUtils.check(_graviteFieldSource);
        XUtils.check(_collider);
        XUtils.check(_visual);

        radius = _radius;
        material = GetComponentInChildren<SpriteRenderer>().material;
    }

    private void FixedUpdate() {
        _lifetime += Time.fixedDeltaTime;
        radius += GetExpansionSpeed() * Time.fixedDeltaTime;

        material.SetFloat(Shader.PropertyToID("_ScaleParam"), Mathf.Pow(radius, 0.35f));
    }

    public float GetRadius()
    {
        return radius;
    }

    private float radius {
        get { return _radius; }
        set {
            _radius = value;
            transform.localScale = new Vector3(_radius, _radius, 1f);
            //_graviteFieldSource.radiusOffset = _radius;
            //_collider.radius = _radius;
            //_visual.transform.localScale = new Vector3(_radius * 10f, _radius * 10f, 1f);
        }
    }

    //Fields
    [SerializeField] private GravityFieldSourceComponent _graviteFieldSource = null;
    [SerializeField] private CircleCollider2D _collider = null;
    [SerializeField] private GameObject _visual = null;

    private float _radius = 1f;
    [SerializeField] float _lifetime = 0f;
    [SerializeField] private AnimationCurve _radiusExpasionSpeed = null;
    public float GetExpansionSpeed()
    {
        return _radiusExpasionSpeed.Evaluate(_lifetime);
    }
}
