﻿using UnityEngine;

public class MovingShip : MonoBehaviour
{
    //Methodss
    void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        Move();
        Rotate();
    }

    private void Move() {
        float move = Input.GetAxis("Vertical");
        _rigidbody.AddForce(transform.right * engineForce * move);
        if (move > 0) {
            flame.SetActive(true);
        }
        else {
            flame.SetActive(false);
        }
    }

    private void Rotate() {
        float rotate = Input.GetAxis("Horizontal");
        _rigidbody.AddTorque(-engineRotationForce * rotate);
    }

    //Fields
    [SerializeField] private GameObject flame = null;

    [SerializeField] private float engineForce = 10f;
    [SerializeField] private float engineRotationForce = 1f;

    private Rigidbody2D _rigidbody = null;
}
