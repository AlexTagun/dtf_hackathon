﻿using UnityEngine;

public class WinBarUIObject : MonoBehaviour
{
    //Methods
    public void set(float inValueRatio) {
        float theValueClamped = Mathf.Clamp(inValueRatio, 0f, 1f);

        Vector2 theAnchor = _valueImage.anchorMax;
        if (isXOrtienter)
            theAnchor.x = theValueClamped;
        else
            theAnchor.y = theValueClamped;
        _valueImage.anchorMax = theAnchor;
    }

    //Fields
    [SerializeField] private RectTransform _valueImage = null;
    [SerializeField] private bool isXOrtienter = false;
}
