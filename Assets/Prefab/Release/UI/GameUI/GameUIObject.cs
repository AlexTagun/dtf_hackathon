﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIObject : MonoBehaviour
{
    void Awake() {
        XUtils.check(_fuelBar);
        XUtils.check(_shipMovement);
    }

    void Update() {
        if (!XUtils.isValid(_shipMovement)) return; //NB: To prevent null access on player kill

        _fuelBar.set(_shipMovement.getFuelValue().getValuePercentFromMinimum());
        _winBar.set((float)_shipMovement.getAcceleratorEnergy() / (float)(_shipMovement.getAcceleratorEnergyMax()));

        if (_laser.GetMaxLaserChargeCount() > 0f) {
            float theRatio = _laser.LaserChargeCount() / _laser.GetMaxLaserChargeCount();
            theRatio = Mathf.Clamp(theRatio, 0f, 1f);
            _laserEnergyBar.set(theRatio);
        }
    }

    //Fields
    [SerializeField] private BarUIObject _fuelBar = null;
    [SerializeField] private BarUIObject _laserEnergyBar = null;
    [SerializeField] private WinBarUIObject _winBar = null;

    [SerializeField] private ShipMovement _shipMovement = null;
    [SerializeField] private Laser _laser = null;
}
