﻿using UnityEngine;

public class WorldMarkerBeaconComponent : MonoBehaviour
{
    private void Awake() {
        XUtils.check(_markerPrefab);
    }

    public WorldMarkerUIObject getMarker(out bool isJustCreated) {
        isJustCreated = false;
        if (null == _marker) {
            _marker = XUtils.createObject(_markerPrefab);
            isJustCreated = true;
        }

        return _marker;
    }

    private void OnDestroy() {
        if (XUtils.isValid(_marker) && XUtils.isValid(_marker.gameObject))
            Object.Destroy(_marker.gameObject);
    }

    //Fields
    [SerializeField] private WorldMarkerUIObject _markerPrefab = null;
    private WorldMarkerUIObject _marker = null;
}
