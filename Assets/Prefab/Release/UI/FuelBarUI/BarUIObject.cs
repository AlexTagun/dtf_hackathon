﻿using UnityEngine;

public class BarUIObject : MonoBehaviour
{
    //Methods
    public void set(float inValueRatio) {
        float theValueClamped = Mathf.Clamp(inValueRatio, 0f, 1f);

        Vector2 theAnchor = _valueImage.anchorMax;
        theAnchor.y = theValueClamped;
        _valueImage.anchorMax = theAnchor;
    }

    //Fields
    [SerializeField] private RectTransform _valueImage = null;
}
