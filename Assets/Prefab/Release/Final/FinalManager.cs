﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class FinalManager : MonoBehaviour
{
    public void showLoose() {
        StartCoroutine(showLooseCoroutine());
    }

    IEnumerator showLooseCoroutine() {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("FinalScene");
    }
}
